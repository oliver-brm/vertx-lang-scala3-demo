package org.example

import com.typesafe.scalalogging.LazyLogging
import io.vertx.core.buffer.Buffer
import io.vertx.core.http.{HttpMethod, HttpServer}
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.handler.{BodyHandler, TemplateHandler}
import io.vertx.ext.web.templ.thymeleaf.ThymeleafTemplateEngine
import io.vertx.ext.web.{Route, Router, RoutingContext}
import io.vertx.lang.scala.ScalaVerticle
import io.vertx.lang.scala.json.*
import io.vertx.lang.scala.*
import io.vertx.scala.core.HttpServerOptions
import org.example.MOTDProviderVerticle.EVENT_BUS_CHANNEL_MOTD

import scala.concurrent.{Future, Promise}
import scala.jdk.CollectionConverters.*
import scala.util.{Failure, Success}
import io.vertx.lang.scala.ImplicitConversions.vertxFutureToScalaFuture
import io.vertx.lang.scala.core.eventbus.Message

import scala.language.implicitConversions

class GreetingServiceVerticle extends ScalaVerticle, LazyLogging:

  private val REQ_PARAM_NAME: String = "name"
  private var httpServer: HttpServer = _

  override def asyncStart: Future[Unit] =
    httpServer = vertx.createHttpServer(HttpServerOptions(port = 9090))
    val router = Router.router(vertx)
    configureRoutes(router)
    logger.info("Greeting Service starting up...")
    httpServer.requestHandler(router)
              .listen
              .map(_ => logger.info("Greeting Service up! -> http://localhost:9090/greeting"))

  override def asyncStop: Future[Unit] =
    logger.info("Greeting Service shutting down...")
    httpServer.close
              .map(_ => logger.info("Greeting Service down!"))

  private def configureRoutes(router: Router): Unit =
    val bodyHandler = BodyHandler.create()
    router.get("/api/motd")
          .handler(httpGetMotdHandler)
    router.post("/api/greeting")
          .handler(bodyHandler)
          .handler(httpPostGreetingHandler)
    router.put("/api/greeting")
          .handler(bodyHandler)
          .handler(httpPutGreetingHandler)
    router.get("/greeting/*")
          .handler(templateHandler)

  private def httpGetMotdHandler(ctx: RoutingContext): Unit =
    for {
      motdMsg <- vertx.eventBus.request[String](EVENT_BUS_CHANNEL_MOTD, Json.obj())
      msg = motdMsg.body()
      _ <- ctx.response
              .putHeader("content-type", "text/html")
              .end(s"<span>$msg</span>")
    } yield ()

  private def httpPostGreetingHandler(ctx: RoutingContext): Unit =
    val name = ctx.request().getFormAttribute(REQ_PARAM_NAME) match
      case null         => config.getString("defaultGreeting", "World")
      case providedName => providedName
    ctx.response()
       .putHeader("content-type", "text/html")
       .end(
         s"""
            |<p class="title">Welcome, $name!</p>
            |<p class="subtitle">Nice to meet you...</p>
            |""".stripMargin)

  private def httpPutGreetingHandler(ctx: RoutingContext): Unit =
    val body = ctx.body.buffer
    vertx.fileSystem.writeFile("body.json", body).asScala onComplete {
      case Success(_)         => ctx.end("DANKE!")
      case Failure(exception) => ctx.response.setStatusCode(500).end(s"Error: ${exception.getMessage}")
    }


  private def templateHandler: TemplateHandler =
    val templateEngine = ThymeleafTemplateEngine.create(vertx)
    TemplateHandler.create(templateEngine)
                   .setIndexTemplate("index.html")
