package org.example

import io.vertx.core.{DeploymentOptions, Verticle}
import io.vertx.lang.scala.ImplicitConversions.{scalaVerticleToJavaVerticle, vertxFutureToScalaFuture}
import io.vertx.lang.scala.*
import io.vertx.lang.scala.json.*

import scala.concurrent.{Future, Promise}
import scala.language.implicitConversions
import scala.util.{Failure, Success}


class MainVerticle extends ScalaVerticle:

  override def asyncStart: Future[Unit] =
    val greetingServiceVerticle = GreetingServiceVerticle()
    val motdProviderVerticle = MOTDProviderVerticle()
    val anotherVerticle = AnotherVerticle()
    val serviceOptions = DeploymentOptions().setConfig(Json.obj(("defaultGreeting", "Scala World")))
    for {
      _ <- vertx.deployVerticle(greetingServiceVerticle, serviceOptions)
      _ <- vertx.deployVerticle(motdProviderVerticle)
      _ <- vertx.deployVerticle(anotherVerticle)
    } yield ()
