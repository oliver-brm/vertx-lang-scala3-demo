package org.example

import com.typesafe.scalalogging.LazyLogging
import io.vertx.core.eventbus.{DeliveryOptions, Message, MessageConsumer}
import io.vertx.lang.scala.ScalaVerticle
import org.example.MOTDProviderVerticle.EVENT_BUS_CHANNEL_MOTD
import org.example.MOTDProviderVerticle.MESSAGES

import scala.concurrent.{Future, Promise}
import scala.util.{Failure, Random, Success}
import io.vertx.lang.scala.*

import io.vertx.lang.scala.ImplicitConversions.vertxFutureToScalaFuture
import scala.language.implicitConversions

class MOTDProviderVerticle extends ScalaVerticle, LazyLogging:

  private var motd: String = _
  private val rnd: Random = Random()
  private var mc: MessageConsumer[Void] = _

  override def asyncStart: Future[Unit] =
    vertx.setPeriodic(5000, _ => motd = randomMessage())
    mc = vertx.eventBus.consumer[Void](EVENT_BUS_CHANNEL_MOTD, (msg: Message[Void]) => msg.reply(motd))
    logger.info("MOTD Provider Verticle up!")
    Future.successful(())

  override def asyncStop: Future[Unit] =
    mc.unregister()
      .map(_ => logger.info("MOTD Provider Verticle down!"))

  private def randomMessage(): String =
    val m = MESSAGES(rnd.nextInt(MESSAGES.length))
    logger.debug(s"""New MOTD: "$m"""")
    m

object MOTDProviderVerticle:
  val EVENT_BUS_CHANNEL_MOTD = "motd"
  val MESSAGES: Vector[String] = Vector(
    "What a great choice!",
    "Vert.x FTW!",
    "Scala FTW!",
    "It's a wonderful day for programming",
    "Vert.x & Scala FTW!",
    "Scala = ❤️",
    "Vert.x = ❤️",
    "Vert.x + Scala = \uD83D\uDC9E"
  )
