package org.example

import com.typesafe.scalalogging.LazyLogging
import io.vertx.core.http.HttpServer
import io.vertx.ext.web.handler.TemplateHandler
import io.vertx.ext.web.templ.thymeleaf.ThymeleafTemplateEngine
import io.vertx.ext.web.{Router, RoutingContext}
import io.vertx.lang.scala.ScalaVerticle
import io.vertx.lang.scala.conv.*

import scala.concurrent.{Future, Promise}
import scala.util.{Failure, Success}

class AnotherVerticle extends ScalaVerticle, LazyLogging:

  override def asyncStart: Future[Unit] =
    logger.info("Another Verticle started!")
    Future.successful(())
