# Vert.x/Scala Demo application

This is a small demo application made with Vert.x and Scala. The website
is made with [Bulma](https://bulma.io) and [HTMX](https://htmx.org).

# Building
```console
sbt assembly
```

# Running
```console
java -jar target/scala-3.2.1/vertx-scala-demo.jar
```

The application should come up quickly. Point your browser to http://localhost:9090/greeting in order to see a demo website.
