import java.util.jar.Attributes.Name

ThisBuild / version := "0.1.0-SNAPSHOT"
ThisBuild / scalaVersion := "3.3.0"

lazy val root = (project in file("."))
  .settings(
    name := "vertx-lang-scala3-demo",
    assembly / assemblyJarName := "vertx-scala-demo.jar",
  )

val mainVerticle = "org.example.MainVerticle"

Compile / mainClass := Some("io.vertx.core.Launcher")
Compile / packageOptions += {
  Package.ManifestAttributes(new Name("Main-Verticle") -> s"scala:$mainVerticle")
}

reStartArgs := Seq("run", s"scala:$mainVerticle")

resolvers += Resolver.mavenLocal

ThisBuild / assemblyMergeStrategy := {
  case PathList("META-INF", "MANIFEST.MF")                  => MergeStrategy.discard
  case PathList("META-INF", xs@_*)                          => MergeStrategy.last
  case PathList("META-INF", "io.netty.versions.properties") => MergeStrategy.last
  case "module-info.class"                                  => MergeStrategy.discard
  case x                                                    =>
    val oldStrategy = (ThisBuild / assemblyMergeStrategy).value
    oldStrategy(x)
}

libraryDependencies ++= Seq(
  "io.vertx" % "vertx-lang-scala3" % "4.3.8-SNAPSHOT",
  "io.vertx" % "vertx-web" % "4.3.7",
  "io.vertx" % "vertx-web-templ-thymeleaf" % "4.3.7",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.5",
  "ch.qos.logback" % "logback-classic" % "1.4.5",
  "io.netty" % "netty-resolver-dns-native-macos" % "4.1.86.Final" % "runtime" classifier "osx-aarch_64",
)
